//
//  GetDocumentsResponse.swift
//  ChinoOSXLibrary
//
//  Created by Chino on 24/04/2017.
//  Copyright © 2017 Chino. All rights reserved.
//

import Foundation

open class GetDocumentsResponse{
    open var documents = [CreateDocumentResponse]()
    public let count: Int
    public let totalCount: Int
    public let limit: Int
    public let offset: Int
    open var ids = [String]()
    
    init(){
        self.documents = []
        self.count = 0
        self.totalCount = 0
        self.limit = 0
        self.offset = 0
        self.ids = []
    }
    
    init(documents: [CreateDocumentResponse], count: Int, totalCount: Int, limit: Int, offset: Int){
        self.documents = documents
        self.count = count
        self.totalCount = totalCount
        self.limit = limit
        self.offset = offset
        self.ids = []
    }
    
    init(ids: [String], count: Int, totalCount: Int, limit: Int, offset: Int){
        self.documents = []
        self.count = count
        self.totalCount = totalCount
        self.limit = limit
        self.offset = offset
        self.ids = ids
    }
}

extension GetDocumentsResponse {
    convenience init(json: [String: Any]) throws {
        
        // Extract documents
        var documents: [CreateDocumentResponse] = []
        
        let ds = json["documents"] as! NSArray
        for value in ds {
            if let document = try? CreateDocumentResponse(json: value as! [String : Any]) {
                documents.append(document)
            }
        }
        
        // Extract count
        guard let count = json["count"] as? Int else {
            throw SerializationError.missing("count")
        }
        
        // Extract totalCount
        guard let totalCount = json["total_count"] as? Int else {
            throw SerializationError.missing("total_count")
        }
        
        // Extract limit
        guard let limit = json["limit"] as? Int else {
            throw SerializationError.missing("limit")
        }
        
        // Extract offset
        guard let offset = json["offset"] as? Int else {
            throw SerializationError.missing("offset")
        }
        
        // Initialize properties
        self.init(documents: documents, count: count, totalCount: totalCount, limit: limit, offset: offset)
    }
    
    convenience init(json_ids: [String: Any]) throws {
        
        // Extract ids
        var ids: [String] = []
        
        let ss = json_ids["IDs"] as! NSArray
        for value in ss {
            ids.append(value as! String)
        }
        
        // Extract count
        guard let count = json_ids["count"] as? Int else {
            throw SerializationError.missing("count")
        }
        
        // Extract totalCount
        guard let totalCount = json_ids["total_count"] as? Int else {
            throw SerializationError.missing("total_count")
        }
        
        // Extract limit
        guard let limit = json_ids["limit"] as? Int else {
            throw SerializationError.missing("limit")
        }
        
        // Extract offset
        guard let offset = json_ids["offset"] as? Int else {
            throw SerializationError.missing("offset")
        }
        
        // Initialize properties
        self.init(ids: ids, count: count, totalCount: totalCount, limit: limit, offset: offset)
    }
}
