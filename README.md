# [COMMUNITY] Chino Swift SDK <!-- omit in toc -->

Swift SDK for Chino.io API

**Community SDK:** Most of the content and the code has been provided 
by third-parties.

**Version**: `1.3`
 
**Useful links**
 - [Full SDK instructions](./INSTRUCTIONS.md)
 - [Chino.io API docs](http://console.test.chino.io/docs/v1)

For issues or questions, please contact [tech-support@chino.io](mailto:tech-support@chino.io).

-------------------------------------------------------------------------------
#### Table of content <!-- omit in toc -->

- [Installation](#installation)
  - [Import directory in Xcode](#import-directory-in-xcode)
  - [Use a package manager](#use-a-package-manager)
- [Usage example](#usage-example)

-------------------------------------------------------------------------------

## Installation

### Import directory in Xcode

1. Drag the folder `ChinoOSXLibrary` within the `ChinoOSXLibrary` directory 
   into your Xcode project. It's the folder marked with `<--` in the tree
   structure:

        ChinoOSXLibrary/
            ┬  ChinoOSXLibrary/  <--
            |    └  ...
            ├  CommonCrypto/
            ├  LICENSE
            └  README.md

### Use a package manager
We provide instructions for the following package managers:

* [CocoaPods](./INSTRUCTIONS.md#cocoapods)
* [Carthage](.INSTRUCTIONS.md#carthage)

## Usage example
Init SDK:
```Swift
import ChinoOSXLibrary

var customerId = "<your-customer-id>"
var customerKey = "<your-customer-key>"
var server = "https://api.test.chino.io/v1"

var chino = ChinoAPI.init(hostUrl: server, customerId: customerId, customerKey: customerKey)
```  

Create a Repository:
```Swift
chino.repositories.createRepository(description: "test_repository_description") { (response) in

    // Response is a function which returns the requested resource (in this case a Repository) and
    // returns an error if the call fails
    var repository: Repository!
    do{
        repository = try response()
    } catch let error {

        // Here you can manage the error
        print((error as! ChinoError).toString())
    }
    ...
}

// <---- ASYNC: While the call is executed the main thread continues to run

```  

*Learn more about the SDK in the [full reference](./INSTRUCTIONS.md).*
